package com.sunny.elasticsearch;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.RestStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * ES处理类
 * @author fengxiangyang
 * @date 2018/6/14
 */
@Slf4j
public class ElasticsearchHandler {
    private final static Logger logger = LoggerFactory.getLogger(ElasticsearchHandler.class);

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    /**
     * 删除索引
     * @param indexName
     * @return
     */
    public boolean deleteIndex(String indexName) {
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(indexName);
        try {
            AcknowledgedResponse deleteIndexResponse = restHighLevelClient.indices().delete(deleteIndexRequest, RequestOptions.DEFAULT);
            return deleteIndexResponse.isAcknowledged();
        } catch (org.elasticsearch.ElasticsearchException e) {
            if (e.status() == RestStatus.NOT_FOUND) {
                log.info("没有找到索引");
                throw new ElasticsearchException(e);
            }
        } catch (IOException e) {
            log.info("删除索引异常");
            throw new ElasticsearchException(e);
        }
        return false;
    }

    /**
     * 创建索引
     * @param indexName
     * @return
     */
    public boolean createIndex(String indexName, XContentBuilder settings, String mappings) {
        CreateIndexRequest indexRequest = new CreateIndexRequest(indexName);
        try {
            indexRequest.settings(settings);
            indexRequest.mapping(mappings, XContentType.JSON);
            CreateIndexResponse createIndexResponse = restHighLevelClient.indices().create(indexRequest, RequestOptions.DEFAULT);
            return createIndexResponse.isShardsAcknowledged();
        } catch (IOException e) {
            log.info("创建索引异常");
            throw new ElasticsearchException(e);
        }
    }

    /**
     * 插入
     *
     * @param indexRequest
     */
    public void insert(IndexRequest indexRequest) {
        try {
            restHighLevelClient.index(indexRequest,RequestOptions.DEFAULT);
        } catch (IOException e) {
            logger.error("插入执行异常：ex={}", e.getMessage());
            throw new ElasticsearchException(e);
        }
    }

    /**
     * 更新
     *
     * @param updateRequest
     */
    public void update(UpdateRequest updateRequest) {
        try {
            restHighLevelClient.update(updateRequest,RequestOptions.DEFAULT);
        } catch (IOException e) {
            logger.error("更新执行异常：ex={}", e.getMessage());
            throw new ElasticsearchException(e);
        }
    }

    /**
     * 删除
     *
     * @param deleteRequest
     */
    public void delete(DeleteRequest deleteRequest) {
        try {
            restHighLevelClient.delete(deleteRequest,RequestOptions.DEFAULT);
        } catch (IOException e) {
            logger.error("删除执行异常：ex={}", e.getMessage());
            throw new ElasticsearchException(e);
        }
    }

    /**
     * 批量操作
     *
     * @param bulkRequest
     */
    public void bulk(BulkRequest bulkRequest) {
        if(bulkRequest.numberOfActions() <= 0){
            return;
        }
        try {

            BulkResponse bulkResponse = restHighLevelClient.bulk(bulkRequest,RequestOptions.DEFAULT);
            if (bulkResponse.hasFailures()) {
                log.info(bulkResponse.buildFailureMessage());
                throw new ElasticsearchException();
            }
        } catch (IOException e) {
            logger.error("批量执行异常：ex={}", e.getMessage());
            throw new ElasticsearchException(e);
        }
    }

    /**
     * 查询
     * @param searchRequest
     */
    public SearchResponse search(SearchRequest searchRequest) {
        try {
            return restHighLevelClient.search(searchRequest,RequestOptions.DEFAULT);
        } catch (IOException e) {
            logger.error("查询异常：ex={}", e.getMessage());
            throw new ElasticsearchException(e);
        }
    }
}

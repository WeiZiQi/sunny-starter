package com.sunny.redis;

/**
 * @author fengxiangyang
 * @date 2018/12/03
 */
@FunctionalInterface
public interface CacheData<T> {
    /**
     * 获取数据
     * @return
     */
    T findData();
}
